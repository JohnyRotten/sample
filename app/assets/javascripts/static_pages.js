$(function(){
	if ($('#micropost_content').length) {
		$('body').on('keyup', '#micropost_content', function() {
			var postLength = $(this).val().length;
			var charactersLeft = 140 - postLength;
			$('#char_counter').text(charactersLeft);

			if(charactersLeft < 0) {
				$('#post_submit').addClass('disabled'); 
			} else if(charactersLeft == 140) {
				$('#post_submit').addClass('disabled');
			} else {
				$('#post_submit').removeClass('disabled');
			}
		});
	}
});