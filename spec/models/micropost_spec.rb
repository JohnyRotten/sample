require 'spec_helper'

describe Micropost do

	let(:user) { FactoryGirl.create(:user) }

	before do
		@micropost = user.microposts.build(content: "Lorem ipsum", user_id: user.id)
	end

	subject { @micropost }

	it { should respond_to(:content) }
	it { should respond_to(:user_id) }
	it { should respond_to(:user) }
	it { expect(@micropost.user).to eq user }

	it { should be_valid }

	describe "when user_id not represent" do 
		before { @micropost.user_id = nil }
		it { should_not be_valid }
	end

	describe "when content too short" do 
		before { @micropost.content = "  " }
		it { should_not be_valid }
	end

	describe "when content too long" do 
		before { @micropost.content = 'a' * 141 }
		it { should_not be_valid }
	end
end